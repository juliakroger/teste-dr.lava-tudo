pergunta 1: Não precisa ser feita alteração na criação do banco de dados, o cliente pode ter mais de um serviço.
pergunta 2: Podemos criar uma table endereço. CREATE DATABASE drLavaTudo; use drLavaTudo;
create table cliente( id int unsigned NOT NULL, nome varchar(60) not null, email varchar(60) not null, dataDeNascimento date not null, telefoneCelular varchar(13) not null, telefoneResidencial varchar(13) not null, primary key(id) );
create table servicos( id int unsigned NOT NULL AUTO_INCREMENT, nome_servico varchar(60) not null, valorFinal double(9,2) not null, custoEmpresa double(9,2) not null, primary key(id) );
create table endereco( id int unsigned NOT NULL AUTO_INCREMENT, cep varchar(8) NOT NULL, rua varchar(20) NOT NULL, numero int(5) NOT NULL, complemento varchar(10), primary key(id) );
create table ordemDeServico( id int unsigned NOT NULL AUTO_INCREMENT, dataContratacao date not null, dataExecucao date not null, cliente_id int unsigned not null, servicos_id int unsigned not null, endereco_id int unsigned not null, primary key(id), foreign key (cliente_id) references cliente(id), foreign key (servicos_id) references servicos(id), foreign key (endereco_id) references endereco(id) );
3. a.Selecione todos os clientes e a quantidade de ordem de serviços select * from cliente; select count(*) from ordemDeServico;
b. Selecione todas as Ordens de Serviços com mais de um serviço select o.id as id, c.nome as cliente, s.nome_servico as servico, dataContratacao as contratacao, dataExecucao as execucao from ordemDeServico as o inner join cliente as c on c.id = o.cliente_id inner join servicos as s on s.id = o.servicos_id where o.cliente_id = 1;
c. Selecione os serviços mais vendidos
d. Atualize o valor final de todos os serviços em 12%
e. Remova a ultima ordem de serviço criada
f. Insira um cliente INSERT INTO cliente (id, nome, email, dataDeNascimento, telefoneCelular, telefoneResidencial) VALUES (1, 'fernanda', 'fernanda@fernanda.com', '2016-10-24', '3199681123', '31996812394');